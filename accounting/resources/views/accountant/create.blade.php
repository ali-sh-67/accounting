@extends('layouts.app')

@section('content')
<div>
    <form method="post" action="{{route('accountantStore')}}" enctype="multipart/form-data">
        @csrf
        <label for="date">تاریخ</label>
        <input type="date" name="date" id="date">
        <label for="group">دسته </label>
        <select name="group" id="group">
            <option value="هزینه">هزینه</option>
            <option value="درآمد">درآمد</option>
        </select>
        <label for="title">عنوان</label>
        <input type="text" name="title" id="title">
        <label for="price">مبلغ</label>
        <input type="number" name="price" id="price">
        <label for="comment">توضیح</label>
        <input type="text" name="comment" id="comment">
        <label for="filePath">فایل ضمیمه</label>
        <input type="file" name="filePath" id="filePath" multiple>
        <input type="submit" value="save">
    </form>
</div>
@endsection
