@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="{{asset('css/content.css')}}">

<div class="table">
    <table>
        <thead>
        <tr>
            <td>ردیف</td>
            <td>تاریخ</td>
            <td>دسته</td>
            <td>عنوان</td>
            <td>مبلغ</td>
            <td>توضیح</td>
            <td>فایل ضمیمه</td>
        </tr>
        </thead>
        <tbody>
        @php($i = 1)
        @foreach($accountants as $accountant)
{{--            @dd($accountant->filePath)--}}
            <tr>
                <td>{{$i}}</td>
                <td>{{$accountant->date}}</td>
                <td>{{$accountant->group}}</td>
                <td>{{$accountant->title}}</td>
                <td>{{$accountant->price}}</td>
                <td>{{$accountant->comment}}</td>
                <td><img width="100px" height="100px" src="{{asset('/files/'.$accountant->filePath)}}"></td>
                <td><a href="{{route('accountantEdit',['id'=>$accountant->id])}}">ویرایش</a></td>
                <td><a href="{{route('accountantDelete',['id'=>$accountant->id])}}">حذف</a></td>
            </tr>
            @php($i++)
        @endforeach
        </tbody>
    </table>

</div>
@endsection
