<?php

namespace App\Http\Controllers;

use App\Models\accountant;
use Illuminate\Http\Request;


class AccountantController extends Controller
{
    public function index(Request $request)
    {
        $accountants= Accountant::all();

        return view('accountant.index',compact('accountants'));
    }

    public function insert()
    {
        return view('accountant.create');
    }

    public function edit(Request $request, $id)
    {
        $accountant = Accountant::find($id);
        return view('accountant.edit',compact('accountant'));
    }

    public function update(Request $request,$id)
    {
        if (isset($request->filePath)) {
            $fileName = time().'.'.$request->filePath->extension();
            $request->filePath->move(public_path('files'), $fileName);
        }
        $accountant = Accountant::find($id);
        $accountant->title = $request->title;
        $accountant->comment = $request->comment;
        $accountant->date = $request->date;
        $accountant->price = $request->price;
        $accountant->group = $request->group;
        if (isset($fileName)){
            $accountant->filePath = $fileName;
        }
        if ($accountant->save()){
            $accountants= Accountant::all();
            return view('accountant.index',compact('accountants'));
        }else{
            return 'fail in update try again';
        }
    }

    public function store(Request $request)
    {
        if (isset($request->filePath)) {
            $fileName = time().'.'.$request->filePath->extension();
            $request->filePath->move(public_path('files'), $fileName);
        }
        $accountant = new Accountant;
        $accountant->title = $request->title;
        $accountant->comment = $request->comment;
        $accountant->date = $request->date;
        $accountant->price = $request->price;
        $accountant->group = $request->group;
        if (isset($fileName)){
        $accountant->filePath = $fileName;
            }
        if ($accountant->save()){
            $accountants= Accountant::all();
            return view('accountant.index',compact('accountants'));
        }else{
            return 'fail in save try again';
        }

    }

    public function delete(Request $request, $id)
    {
        $accountant=Accountant::find($id);
        if ($accountant){
            $accountant->delete();
            $accountants=Accountant::all();
            return view('accountant.index',compact('accountants'));
        }else{
            return "unable to delete";
        }

    }

    public function report(){
        $reports=Accountant::all();
        return view('accountant.report',compact('reports'));
//        return Response::json($reports, 200, [], JSON_UNESCAPED_UNICODE);


    }
}
