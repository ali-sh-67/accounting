<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AccountantController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

//Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');
Route::controller(AccountantController::class)->middleware('auth')->prefix('/')->group(function (){
    Route::get('/','index')->name('accountantIndex');
    Route::get('/insert','insert')->name('accountantInsert');
    Route::get('/edit/{id}','edit')->name('accountantEdit');
    Route::post('/update/{id}','update')->name('accountantUpdate');
    Route::post('/store','store')->name('accountantStore');
    Route::get('/delete/{id}','delete')->name('accountantDelete');
    Route::get('/report','report')->name('accountantReport');

});

