@extends('layouts.app')

@section('content')
    <div>
        <form method="post" action="{{route('accountantUpdate',['id'=> $accountant->id])}}" enctype="multipart/form-data">
            @csrf
            <label for="date">تاریخ</label>
            <input type="date" name="date" id="date" value="{{$accountant->date}}">
            <label for="group">دسته </label>
            <select name="group" id="group">
                <option>{{$accountant->group}}</option>
                <option value="cost">هزینه</option>
                <option value="income">درآمد</option>
            </select>
            <label for="title">عنوان</label>
            <input type="text" name="title" id="title" value="{{$accountant->title}}">
            <label for="price">مبلغ</label>
            <input type="number" name="price" id="price" value="{{$accountant->price}}">
            <label for="comment">توضیح</label>
            <input type="text" name="comment" id="comment" value="{{$accountant->comment}}">
            <label for="filePath">فایل ضمیمه: </label>
              {{$accountant->filePath}}
            <input type="file" name="filePath" id="filePath" multiple>
            <input type="submit" value="update">
        </form>
    </div>
@endsection
